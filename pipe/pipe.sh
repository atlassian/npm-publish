#!/bin/bash
#
# npm deploy pipe
#
# Required globals:
#   NPM_TOKEN
#
# Optional globals:
#   FOLDER
#   EXTRA_ARGS
#   DEBUG
#

source "$(dirname "$0")/common.sh"

info "Starting pipe execution..."

# required parameters
NPM_TOKEN=${NPM_TOKEN:?'NPM_TOKEN variable missing.'}

# optional parameters
NPM_REGISTRY_AUTH_URL=${NPM_REGISTRY_AUTH_URL:=""}
FOLDER=${FOLDER:="."}
EXTRA_ARGS=${EXTRA_ARGS:=""}
DEBUG=${DEBUG:="false"}

DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  set -x
  DEBUG_ARGS="--verbose"
fi

# Getting package.json configuration
package_json=$(cat "${FOLDER}"/package.json)
debug "package.json file: ${package_json}"

package_name=$(echo "${package_json}" | jq -r '.name')
package_version=$(echo "${package_json}" | jq -r '.version')
npm_registry_url=$(echo "${package_json}" | jq -r '.publishConfig.registry // empty')

# Getting registry url

if [[ -z "$npm_registry_url" ]]; then
  website_url="https://www.npmjs.com/package/$package_name/v/$package_version"
  npm_registry_url="https://registry.npmjs.org"
else
  website_url=${npm_registry_url}
fi
info "Registry URL: $npm_registry_url"

# Setting NPM auth
info "Setting npm auth configuration..."

if [[ -z "${NPM_REGISTRY_AUTH_URL}" ]]; then
  npm_auth_url=$(node -p "require('url').parse('$npm_registry_url').host")
else
  npm_auth_url=${NPM_REGISTRY_AUTH_URL}
fi

set +x
npm config set //${npm_auth_url}/:_authToken "${NPM_TOKEN}"

if [[ "${DEBUG}" == "true" ]]; then
 set -x
fi

npm config set registry ${npm_registry_url}
debug "~/.npmrc file: $(cat ~/.npmrc | sed 's|'${NPM_TOKEN}'|******|')"

# Executing npm publish
info "Publishing package..."
run npm publish ${FOLDER} ${DEBUG_ARGS} ${EXTRA_ARGS}

if [[ "${status}" -eq 0 ]]; then
  success "Package \"$package_name@$package_version\" published successfully. URL: $website_url"
else
  fail "Failed to publish package \"$package_name@$package_version\"."
fi
