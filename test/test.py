from copy import deepcopy
from datetime import datetime
import json
import os

from bitbucket_pipes_toolkit.test import PipeTestCase


BASIC_PACKAGE_JSON = {
    "name": "npm-test-pipes-atlassian",
    "version": "2.0.0-20190325032958",
    "description": "A description",
    "main": "index.js",
    "author": "atlassian",
    "license": "ISC"
}


class NPMPublishPipeTestCase(PipeTestCase):
    package_json_path = './test/package.json'

    def setUp(self):
        self.package_json = deepcopy(BASIC_PACKAGE_JSON)

    def tearDown(self):
        with open(self.package_json_path, 'w') as f:
            json.dump(self.package_json, f, indent=4)

    def test_publish(self):
        with open(self.package_json_path, 'w') as f:
            version_parts = self.package_json['version'].split('-')
            current_ts = datetime.strftime(datetime.utcnow(), '%Y%m%d%H%M%S')
            self.package_json['version'] = f'{version_parts[0]}-{current_ts}'

            json.dump(self.package_json, f, indent=4)

        result = self.run_container(environment={
            'NPM_TOKEN': os.getenv('NPM_TOKEN'),
            'FOLDER': './test',
            'DEBUG': 'true',
        })
        self.assertIn("http fetch PUT 200 https://registry.npmjs.org", result)
        self.assertIn('status=0', result)

        result_package_name = f'{self.package_json["name"]}@{self.package_json["version"]}'
        self.assertIn(f'Package "{result_package_name}" published successfully', result)

    def test_publish_existing_version(self):
        result = self.run_container(environment={
            'NPM_TOKEN': os.getenv('NPM_TOKEN'),
            'FOLDER': './test',
            'DEBUG': 'true',
        })
        self.assertIn('status=1', result)
        self.assertIn(f'403 Forbidden - PUT https://registry.npmjs.org/{self.package_json["name"]} - '
                      f'You cannot publish over the previously published versions',
                      result)


class NPMPublishCustomRegistryTestCase(PipeTestCase):
    package_json_path = 'test/package.json'
    stub_registry_image = 'sonatype/nexus3'
    test_port = '8079'

    def setUp(self):
        self.package_json = deepcopy(BASIC_PACKAGE_JSON)

    def tearDown(self):
        with open(self.package_json_path, 'w') as f:
            json.dump(BASIC_PACKAGE_JSON, f, indent=4)

    def test_publish_with_custom_registry_url(self):
        package_json = deepcopy(self.package_json)

        npm_registry_auth_url = '127.0.0.1:4534'

        npm_registry_url = f'http://{npm_registry_auth_url}/fake_repo'
        with open(self.package_json_path, 'w') as f:
            version_parts = package_json['version'].split('-')
            current_ts = datetime.strftime(datetime.utcnow(), '%Y%m%d%H%M%S')
            package_json['version'] = f'{version_parts[0]}-{current_ts}'
            package_json['publishConfig'] = {'registry': npm_registry_url}
            json.dump(package_json, f, indent=4)

        result = self.run_container(environment={
            'NPM_TOKEN': os.getenv('NPM_TOKEN'),
            'NPM_REGISTRY_AUTH_URL': npm_registry_auth_url,
            'FOLDER': './test',
            'DEBUG': 'true',

        })
        self.assertIn(f'~/.npmrc file: //{npm_registry_auth_url}/:_authToken',
                      result)
