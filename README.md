# Bitbucket Pipelines Pipe: NPM Publish

This pipe can publish your npm package defined in `package.json` to [npmjs.com][npmjs.com] or any other npm-like registry. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/npm-publish:1.1.1
  variables:
    NPM_TOKEN: '<string>'
    # FOLDER: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # NPM_REGISTRY_AUTH_URL: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable              | Usage                                                                                                                                  |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| NPM_TOKEN (\*)        | The npm Auth Token                                                                                                                     |
| FOLDER                | Relative PATH to the folder containing the `package.json` file, i.e `./dist`. Default to the current directory: `.`                    |
| EXTRA_ARGS            | Extra arguments to be passed to the npm publish command (see [npm CLI docs][npm CLI docs] for more details). Defaults to unset.        |
| NPM_REGISTRY_AUTH_URL | Authentication url for publish. Default: '' (empty string). If empty, the host of the registry url from the package.json will be used. |
| DEBUG                 | Turn on extra debug information. Default: `false`.                                                                                     |

_(\*) = required variable._

## Details

By default, the pipe publishes the contents of the current build directory. Use a `.npmignore` file to keep stuff out of your package. If there's no `.npmignore` file, 
but there is a `.gitignore` file, then npm will ignore the stuff matched by the `.gitignore` file. 
By default, the pipe publishes to [npmjs.com][npmjs.com]. You can publish to any other registry by adding a `publishConfig.registry` key in your `package.json`.
See [npm documentation][npm documentation] for more details.

## Prerequisites

npm Auth Token is necessary to use this pipe.

- To obtain token Log in to your NPM account and follow the [official NPM tutorial][official NPM tutorial].
- Add the generated token as a [secured environment variable][secured environment variable] in Bitbucket Pipelines.

## Examples

### Basic example 
Publishes a package from the current directory to [npmjs.com][npmjs.com] using the token generated in NPM. See more details: [official NPM tutorial][official NPM tutorial].

```yaml
script:
  - pipe: atlassian/npm-publish:1.1.1
    variables:
      NPM_TOKEN: $NPM_TOKEN
```

### Advanced example 
Publishes a package from the `package1` directory to a private registry. 

```yaml
script:
  - pipe: atlassian/npm-publish:1.1.1
    variables:
      NPM_TOKEN: $NPM_TOKEN
      FOLDER: './package1'
```

_package.json_ file example:
```json
{
  "name": "package",
  "version": "1.0.11",
  "description": "Description",
  "publishConfig":{"registry":"my-internal-registry.local"}
}
```

### Advanced example 2
Publishes a package from the `package1` directory to a private registry. 
Support authentication url environment variable in case of npm registry url host cannot be used for authentication. 

```yaml
script:
  - pipe: atlassian/npm-publish:1.1.1
    variables:
      NPM_TOKEN: $NPM_TOKEN
      NPM_REGISTRY_AUTH_URL: 'my-internal-registry.local/path'
      FOLDER: './package1'
```

_package.json_ file example:
```json
{
  "name": "package",
  "version": "1.0.11",
  "description": "Description",
  "publishConfig":{"registry":"my-internal-registry.local/path/repo_name"}
}
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,npm
[npmjs.com]: https://npmjs.com
[npm CLI docs]: https://docs.npmjs.com/cli/publish
[npm documentation]: https://docs.npmjs.com/misc/developers#keeping-files-out-of-your-package
[official NPM tutorial]: https://docs.npmjs.com/getting-started/working_with_tokens
[secured environment variable]: https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables
