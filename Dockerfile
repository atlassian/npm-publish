FROM node:18-alpine3.18

RUN apk --no-cache add \
    jq=1.6-r4 \
    bash=5.2.15-r5 && \
    wget -qP / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
